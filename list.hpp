///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// Header file for List class
///
/// @file list.hpp
/// @version 1.0
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   06 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

class DoubleLinkedList {
protected:
   Node* head = nullptr;
   Node* tail = nullptr;

public:
   const bool empty() const;
   
   inline unsigned int size() const{
      unsigned int c = 0;
      Node* countNode = head;
      while(countNode != nullptr){
         c++;
         countNode = countNode->next;
      }
      return c;
   };

   void  push_front( Node* newNode );
   void  push_back( Node* newNode );
   Node* pop_front() ;
   Node* pop_back();
  
   Node* get_first() const;
   Node* get_last() const;
   Node* get_next( const Node* currentNode ) const;
   Node* get_prev( const Node* currentNode ) const;
  
   void  insert_after( Node* currentNode, Node* newNode );
   void  insert_before( Node* currentNode, Node* newNode );

   void  swap( Node* node1, Node* node2 );
   const bool isSorted() const;
   void  insertionSort();
};

