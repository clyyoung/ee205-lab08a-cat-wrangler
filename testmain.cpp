///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file testmain.cpp
/// @version 1.0
///
/// The test main loop for this lab
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   06 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "cat.hpp"
#include "list.hpp"

using namespace std;

// The purpose of this main() function is to exercise your
// DoubleLinkedList and quickly visualize the correct operation
// of the methods.

int main() {
   Node node;
   DoubleLinkedList testList;

   Cat::initNames();

   

   for(int i = 0; i < 8; i++){
      testList.push_back(Cat::makeCat());
//      for( Cat* cat = (Cat*) testList.get_last(); cat!= nullptr; cat = (Cat*)testList.get_prev(cat)){
//         cout << cat->getInfo() << endl;
//      }
//      cout << "=========" << endl;
   }      
      for( Cat* cat = (Cat*) testList.get_last(); cat!= nullptr; cat = (Cat*)testList.get_prev(cat)){
         cout << cat->getInfo() << endl;
      }
         cout << "=========" << endl;

      testList.swap(testList.get_first(), testList.get_last());

      testList.push_back(Cat::makeCat());

      for( Cat* cat = (Cat*) testList.get_last(); cat!= nullptr; cat = (Cat*)testList.get_prev(cat)){
         cout << cat->getInfo() << endl;
      }

/*
//   for(int i = 0; i < 8; i++){
      testList.pop_back();
      for( Cat* cat = (Cat*) testList.get_last(); cat!= nullptr; cat = (Cat*)testList.get_prev(cat)){
         cout << cat->getInfo() << endl;
      }
         cout << "=========" << endl;

      testList.push_back(Cat::makeCat());
      for( Cat* cat = (Cat*) testList.get_last(); cat!= nullptr; cat = (Cat*)testList.get_prev(cat)){
         cout << cat->getInfo() << endl;
      }

  // } 
*/
/*	cout << "Welcome to Cat Wrangler" << endl;

	Cat::initNames();

	Cat* newCat = Cat::makeCat();

	cout << newCat->getInfo() << endl;
   cout << "========" << endl;


	DoubleLinkedList list = DoubleLinkedList();

	// Put 16 cats in my list
	for( int i = 0 ; i < 8 ; i++ ) {
		list.push_front( Cat::makeCat() );
		list.push_back( Cat::makeCat() );
	}
	
   for( Cat* cat = (Cat*)list.get_first() ; cat != nullptr ; cat = (Cat*)list.get_next( cat )) {
		cout << cat << endl;
	}
   
   cout << "========" << endl;
	
   for( Cat* cat = (Cat*)list.get_first() ; cat != nullptr ; cat = (Cat*)list.get_next( cat )) {
		cout << cat->getInfo() << endl;
	}
   
   cout << "========" << endl;

   list.insertionSort();  // They should be sorted by name

   for( Cat* cat = (Cat*)list.get_first() ; cat != nullptr ; cat = (Cat*)list.get_next( cat )) {
		cout << cat << endl;
	}   

   cout << "========" << endl;

   for( Cat* cat = (Cat*)list.get_first() ; cat != nullptr ; cat = (Cat*)list.get_next( cat )) {
		cout << cat->getInfo() << endl;
	}
   
   cout << "========" << endl;

   Cat* first = (Cat*) list.get_first();
   cout << first->getInfo() << endl;
   Cat* last = (Cat*) list.get_last();
   cout << last->getInfo() << endl;
*/
   return 0;
}

