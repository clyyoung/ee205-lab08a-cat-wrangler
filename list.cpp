///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// File for List class
///
/// @file list.cpp
/// @version 1.0
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   06 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <stdlib.h>
#include <cassert>
#include "list.hpp"

using namespace std;

const bool DoubleLinkedList::empty() const{
   return (head == nullptr && tail == nullptr);
}

void DoubleLinkedList::push_front(Node* newNode){
   if(newNode == nullptr)
      return;

   if(!empty()){    //list is not empty
      newNode->next = head;
      newNode->prev = nullptr;
      head->prev = newNode;
      head = newNode;
   } else {                //list is empty, head and tail are at same node
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
}

Node* DoubleLinkedList::pop_front(){
   if(head == nullptr && tail == nullptr)
      return nullptr;
   Node* temp = head;      //temp node holds head data

   if(head == tail){          //only one node in list
      head = nullptr;
      tail = nullptr;
   } else {
      head = temp->next;      //new head is the node after temp
      head->prev = nullptr;   //before head it should be nullptr
   }

   return temp;
}

Node* DoubleLinkedList::get_first() const{
   return head;
}

Node* DoubleLinkedList::get_next(const Node* currentNode) const{
   Node* node = head;
   while(node != currentNode){
      node = node->next;
   }
   return node->next;
}

void DoubleLinkedList::push_back(Node* newNode){
   if(newNode == nullptr)
      return;

   if(!empty()){    //list is not empty
      newNode->next = nullptr;
      newNode->prev = tail;
      tail->next = newNode;
      tail = newNode;
   } else {                //list is empty, head and tail are at same node
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
}

Node* DoubleLinkedList::pop_back(){
   if(head == nullptr && tail == nullptr) //return nullptr is list is empty
      return nullptr;
   Node* temp = tail;      //temp node holds tail data
   
   if(head == tail){       //only one node in list
      head = nullptr;
      tail = nullptr;
   } else {
      tail = temp->prev;      //new tail is the node before temp
      tail->next = nullptr;   //after tail it should be nullptr
   }
   
   return temp;            //return temp node with old tail's data
}

Node* DoubleLinkedList::get_last() const{
   return tail;
}

Node* DoubleLinkedList::get_prev(const Node* currentNode) const{
   Node* node = head;            //start at beginning of list
   while(node != currentNode){   //go through list until at currentNode
      node = node->next;
   }
   return node->prev;            //return node before currentNode
}

void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode){
   if(currentNode == nullptr && head == nullptr){
      push_front(newNode);    //list is empty, so just add node to front
      return;
   }

   if(newNode == currentNode || newNode == nullptr)
      return;                 //newNode is same as current or nullptr

   if(currentNode != nullptr && head == nullptr)
      assert(false);          //currentNode cannot hold data since list is empty

   if(currentNode == nullptr && head != nullptr)
      assert(false);          //currentNode must hold data if list isn't empty

   if(tail != currentNode){   //NOT inserting at end of list
      newNode->next = currentNode->next;
      currentNode->next = newNode;
      newNode->prev = currentNode;
      newNode->next->prev = newNode;
   } else {                   //inserting at end of list
      push_back(newNode);
   }
}

void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode){
   if(currentNode == nullptr && head == nullptr){
      push_front(newNode);    //list is empty, so just add node to front
      return;
   }

   if(currentNode == nullptr){
      push_back(newNode);
      return;
   }

   if(newNode == currentNode || newNode == nullptr)
      return;                 //newNode is same as current or nullptr

   if(currentNode != nullptr && head == nullptr)
      assert(false);          //currentNode cannot hold data since list is empty

   if(currentNode == nullptr && head != nullptr)
      assert(false);          //currentNode must hold data if list isn't empty

   if(head != currentNode){   //NOT inserting at beginning of list
      newNode->prev = currentNode->prev;
      currentNode->prev = newNode;
      newNode->next = currentNode;
      newNode->prev->next = newNode;
   } else {                   //inserting at beginning of list
      push_front(newNode);
   }
}

void DoubleLinkedList::swap(Node* node1, Node* node2){
   //cases where nothing needs to be done:
   //if nodes are the same, it list is empty, if only one node in list
   if(node1 == node2 || empty() || size() == 1)
      return;

   //two nodes only, so must be either head or tail
   if(size() == 2){
      if(node1 == head && node2 == tail){
         head = node2;
         tail = node2;
      }
      else if(node1 == tail && node2 ==head){
         head = node1;
         tail = node2;
      }

      //ensure all pointers to prev and next are set correctly
      head->prev = nullptr;
      head->next = tail;
      tail->next = nullptr;
      tail->prev = head;

   } else { //list has more than 2 nodes   
         //both nodes are either head or tail
         if((node1 == head && node2 == tail) || (node1 == tail && node2 == head)){
            if(node1 == head && node2 == tail){
               Node* next1 = head->next;
               Node* prev2 = tail->prev;

               head->next = nullptr;
               head->prev = prev2;
               prev2->next = head;
               tail->prev = nullptr;
               tail->next = next1;
               next1->prev = tail;
               head = node2;
               tail = node1;
            } else if(node1 == tail && node2 == head){
               Node* next2 = head->next;
               Node* prev1 = tail->prev;

               head->next = nullptr;
               head->prev = prev1;
               prev1->next = head;
               tail->prev = nullptr;
               tail->next = next2;
               next2->prev = tail;
               head = node1;
               tail = node2;
            }

         //one node is either head or tail
         } else if(node1 == head || node1 == tail || node2 == head || node2 == tail){
            //save all next and prev nodes
            Node* prev1 = node1->prev;
            Node* prev2 = node2->prev;
            Node* next1 = node1->next;
            Node* next2 = node2->next;

            //each possibility needs to consider if nodes are adjacent or not
            if(node1 == head){
               if(node1->next == node2){
                  node1->next = next2;
                  node1->prev = node2;
                  next2->prev = node1;
                  node2->next = node1;
                  node2->prev = nullptr;
                  head = node2;
               } else {
                  node1->next = next2;
                  node1->prev = prev2;
                  next2->prev = node1;
                  prev2->next = node1;
                  node2->next = next1;
                  node2->prev = nullptr;
                  next1->prev = node2;
                  head = node2;
               }
            } else if(node1 == tail){
               if(node2->next == node1){
                  node1->prev = prev2;
                  node1->next = node2;
                  prev2->next = node1;
                  node2->prev = node1;
                  node2->next = nullptr;
                  tail = node2;
               } else {
                  node1->prev = prev2;
                  node1->next = next2;
                  prev2->next = node1;
                  next2->prev = node1;
                  node2->prev = prev1;
                  node2->next = nullptr;
                  prev1->next = node2;
                  tail = node2;
               }
            } else if(node2 == head){
               if(node2->next == node1){
                  node2->next = next1;
                  node2->prev = node1;
                  next1->prev = node2;
                  node1->next = node2;
                  node1->prev = nullptr;
                  head = node1;
               } else {
                  node2->next = next1;
                  node2->prev = prev1;
                  next1->prev = node2;
                  prev1->next = node2;
                  node1->next = next2;
                  node1->prev = nullptr;
                  next2->prev = node1;
                  head = node1;
               }
            } else if(node2 == tail){
               if(node1->next == node2){
                  node2->prev = prev1;
                  node2->next = node1;
                  prev1->next = node2;
                  node1->prev = node2;
                  node1->next = nullptr;
                  tail = node1;                
               } else {
                  node2->prev = prev1;
                  node2->next = next1;
                  prev1->next = node2;
                  next1->prev = node2;
                  node1->prev = prev2;
                  node1->next = nullptr;
                  prev2->next = node1;
                  tail = node1;
               }
            }

         //nodes are neither head or tail
         } else {
            //save all next and prev nodes
            Node* prev1 = node1->prev;
            Node* prev2 = node2->prev;
            Node* next1 = node1->next;
            Node* next2 = node2->next;
            
            bool nearHead1 = false;
            bool nearTail1 = false;
            bool nearHead2 = false;
            bool nearTail2 = false;

            if(node1->prev == head)
               nearHead1 = true;
            else if(node2->prev == head)
               nearHead2 = true;
            else if(node1->next == tail)
               nearTail1 = true;
            else if(node2->next == tail)
               nearTail2 = true;

            //adjacent nodes
            if(node1->next == node2){
               node1->next = next2;
               node1->prev = node2;
               next2->prev = node1;
               node2->next = node1;
               node2->prev = prev1;
               prev1->next = node2;
            } else if(node2->next == node1){
               node2->next = next1;
               node2->prev = node1;
               next1->prev = node2;
               node1->next = node2;
               node1->prev = prev2;
               prev2->next = node1;
            //non-adjacent nodes
            } else {
               node1->next = next2;
               node1->prev = prev2;
               next2->prev = node1;
               prev2->next = node1;
               node2->next = next1;
               node2->prev = prev1;
               prev1->next = node2;
               next1->prev = node2;
            }

               if(nearHead1)
                  head->next = node2;
               if(nearHead2)
                  head->next = node1;
               if(nearTail1)
                  tail->prev = node2;
               if(nearTail2)
                  tail->prev = node1;

         }
   }
}

const bool DoubleLinkedList::isSorted() const{
   if(size()<=1)   //if list is empty or only has one item
      return true;

   for(Node* i = head; i->next != nullptr; i = i->next){
      if(*i > *i->next)
         return false;
   }

   return true;
}

void DoubleLinkedList::insertionSort(){
   if(isSorted())
      return;

   if(empty() || size() == 1) //if list is empty or only has one item
      return;

   for(Node* i = head; i->next != nullptr; i = i->next){    //outer loop
      Node* minNode = i;   

      for(Node* j = i->next; j != nullptr; j = j->next){    //inner loop
         if(*minNode > *j)    //if newest minimum is found in list
            minNode = j;      //update minimum node
      }
      swap(i, minNode);       //swap current node with smallest node found
      i = minNode;            //update current node
   }
}
